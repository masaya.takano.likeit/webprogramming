<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
       <head>
         <meta  charset="utf-8">
           <title>ユーザ削除</title>
           <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
           <link rel="stylesheet" href="style.css">

            </head>
       <body>
           <header>
              <div class="col-sm-10">
                  <a>${userInfo.name}</a>
               </div>
            <div class="col-sm-2">
                <a href="DeleteServlet">ログアウト</a><br>
               </div>


           </header>

           <div class="main">
           <h1>ユーザ削除確認</h1>
           </div>
	<form method="post" action="DeleteServlet">
               <div class="del">

        　<dl>
				<dt>ログインID</dt>
				<dd>${user.loginId}</dd>
			</dl>
	<input type ="hidden" name="id" value="${user.id}" >
            <p>を本当に削除してよろしいでしょうか。</p>
           </div>

           <div class="flex">

               <p><button type="button" onclick="history.back()"> キャンセル </button></p>
               <p><input type="submit" value="  OK   "></p>

           </div>
           </form>
      </body>
  </html>