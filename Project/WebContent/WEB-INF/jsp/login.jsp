<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>ログイン</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="/WEB-INF/style.css">

</head>
<body>
	<div class="main">
		<h1>ログイン画面</h1>

		<form action="LoginServlet" method="post">
			<dl>
				<dt>ログインID</dt>

				<dd>
					<input type="text" name="loginId" id="inputLoginId" required
						autofocus>
				</dd>
			</dl>

			<dl>
				<dt>パスワード</dt>

				<dd>
					<input type="password" name="password" id="inputPassword" required>
				</dd>
			</dl>
			<input type="submit" value="   ログイン   ">
		</form>
		<span class="label label-danger">${errMsg}</span>
	</div>
</body>
</html>