<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
       <head>
         <meta  charset="utf-8">
           <title>ユーザ新規登録</title>
           <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
           <link rel="stylesheet" href="style.css">

            </head>
       <body>
           <div class="main">
           <header>
              <div class="col-sm-10">
                  <a>${userInfo.name}</a>
               </div>
            <div class="col-sm-2">
                <a href="DeleteServlet">ログアウト</a><br>
               </div>


           </header>

               <h1>ユーザ新規登録</h1>
	<span class="label label-danger">${ErrMsg}</span>
               <form method="post" action="NewServlet">
               <dl>
                   <dt>ログインID</dt>
                   <dd><input type="text" name="loginId" value="${logErr}" size="24"></dd>
                   </dl>

                   <dl>
                       <dt>パスワード</dt>
                       <dd><input type="password" name="password" value="" size="24"></dd>

                   </dl>

                   <dl>
                       <dt>パスワード(確認)</dt>
                       <dd><input type="password" name="password2" value="" size="24"></dd>
                   </dl>

               <dl>
                   <dt>ユーザ名</dt>
                   <dd><input type="text" name="name" value="${nameErr}" size="24"></dd>
                   </dl>

               <dl>
                   <dt>生年月日</dt>
                   <dd><input type="date" name="birthDate" value="${birthErr}"></dd>
                   </dl>
               <input type="submit" value="   登録   " style="width: 10%;">
               </form>
           </div>
           <a href="UserListServlet">戻る</a><br>

</body>
  </html>