<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>ユーザ情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="style.css">

</head>
<body>
	<div class="main">
		<header>
			<div class="col-sm-10">
				<a>${userInfo.name}</a>
			</div>
			<div class="col-sm-2">
				<a href="LogoutServlet">ログアウト</a><br>
			</div>


		</header>

		<h1>ユーザ情報更新</h1>
		<span class="label label-danger">${ErrMsg}</span>

		<form method="post" action="ResetServlet">
			<dl>
				<dt>ログインID</dt>
				<dd>${user.loginId}</dd>
			</dl>
			<input type="hidden" name="id" value="${user.id}">
			<dl>
				<dt>パスワード</dt>
				<dd>
					<input type="password" name="password" value="" size="24">
				</dd>
			</dl>

			<dl>
				<dt>パスワード(確認)</dt>
				<dd>
					<input type="password" name="password2" value="" size="24">
				</dd>
			</dl>
			<dl>
				<dt>ユーザ名</dt>
				<dd>
					<input type="text" name="name" value="${user.name}"size="24">
				</dd>
			</dl>

			<dl>
				<dt>生年月日</dt>
				<dd>
					<input type="date" name="birthDate" value="${user.birthDate}">
				</dd>
			</dl>

			<input type="submit" value="　  更新  　" style="width: 40%;">
		</form>
	</div>
	<a href="UserListServlet">戻る</a>
	<br>

</body>
</html>