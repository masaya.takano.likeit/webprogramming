
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>ログイン</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="style.css">

</head>
<body>
	<div class="main">
		<header>
			<div class="col-sm-10">
				<a>${userInfo.name}</a>
			</div>
			<div class="col-sm-2">
				<a href="LogoutServlet">ログアウト</a><br>
			</div>


		</header>

		<h1>ユーザ情報詳細参照</h1>


			<dl>
				<dt>ログインID</dt>
				<dd>${user.loginId}</dd>
			</dl>
			<dl>
				<dt>ユーザー名</dt>
				<dd>${user.name}</dd>
			</dl>
			<dl>
				<dt>生年月日</dt>
				<dd>${user.birthDate}</dd>
			</dl>
			<dl>
				<dt>登録日時</dt>
				<dd>${user.createDate}</dd>
			</dl>
			<dl>
				<dt>更新日時</dt>
				<dd>${user.updateDate}</dd>
			</dl>



	</div>
	<a href="UserListServlet">戻る</a>
	<br>

</body>
</html>