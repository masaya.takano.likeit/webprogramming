<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>ログイン</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="style.css">

</head>
<body>
	<div class="main">
		<header>
			<div class="col-sm-10">

				<a>${userInfo.name}</a>

			</div>
			<div class="col-sm-2">
				<a href="LogoutServlet">ログアウト</a><br>
			</div>


		</header>

		<h1>ユーザ一覧</h1>

		<div class="new">
			<a href="NewServlet">新規登録</a><br>
		</div>
		<form method="post" action="UserListServlet">
			<dl>
				<dt>ログインID</dt>
				<dd>
					<input type="text" name="loginId" id="inputLoginId">
				</dd>
			</dl>
			<dl>
				<dt>ユーザ名</dt>
				<dd>
					<input type="text" name="name" value="" size="40">
				</dd>
			</dl>
			<dl>
				<dt>生年月日</dt>
				<dd>
					<input type="date" name="birthDate">〜<input type="date"
						name="birthDate2">
				</dd>
			</dl>

			<div class="ken">
				<input class="ken" type="submit" value="       検索       ">
				<hr>

			</div>
		</form>

		<div>
			<table>
				<thead>
					<tr>
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${userList}">
						<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>


							<c:if test="${userInfo.loginId == 'admin'}">

								<td><a href="UserDetailServlet?id=${user.id}">詳細</a> <a
									href="ResetServlet?id=${user.id}">更新</a> <a
									href="DeleteServlet?id=${user.id}">削除</a></td>
							</c:if>

							<c:if test="${userInfo.loginId != 'admin'}">

								<td><a href="UserDetailServlet?id=${user.id}">詳細</a> <c:if
										test="${user.loginId ==userInfo.loginId}">
										<a href="ResetServlet?id=${user.id}">更新</a>
									</c:if></td>
							</c:if>


						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
</body>
</html>