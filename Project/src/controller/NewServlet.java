package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class NewServlet
 */
@WebServlet("/NewServlet")
public class NewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		if (loginId.equals("") || password.equals("") || password2.equals("") || name.equals("")
				|| birthDate.equals("")) {
			request.setAttribute("ErrMsg", "入力された内容は正しくありません");
			request.setAttribute("logErr", loginId);
			request.setAttribute("nameErr", name);
			request.setAttribute("birthErr", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
			dispatcher.forward(request, response);

		} else if (!(password.equals(password2))) {
			request.setAttribute("ErrMsg", "入力された内容は正しくありません");
			request.setAttribute("logErr", loginId);
			request.setAttribute("nameErr", name);
			request.setAttribute("birthErr", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
			dispatcher.forward(request, response);

		} else if (!(loginId.equals(""))||name.equals("")) {
			UserDao userDao = new UserDao();
			User user = userDao.findByLoginId(loginId);

			if (user != null) {
				request.setAttribute("ErrMsg", "入力された内容は正しくありません");
				request.setAttribute("logErr", loginId);
				request.setAttribute("nameErr", name);
				request.setAttribute("birthErr", birthDate);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
				dispatcher.forward(request, response);

				return;

		} else {

			UserDao UserDao = new UserDao();
			UserDao.registUser(loginId, password, name, birthDate);
			response.sendRedirect("UserListServlet");
		}

	}
	}
	}
