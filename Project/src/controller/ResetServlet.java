package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class ResetServlet
 */
@WebServlet("/ResetServlet")
public class ResetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ResetServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");

		UserDao Dao = new UserDao();
		User user = UserDao.findById(id);

		request.setAttribute("user", user);

		RequestDispatcher dispatcheer = request.getRequestDispatcher("/WEB-INF/jsp/reset.jsp");
		dispatcheer.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String id = request.getParameter("id");

		if (name.equals("") || birthDate.equals("")) {
			request.setAttribute("ErrMsg", "入力された内容は正しくありません");
			UserDao Dao = new UserDao();
			User user = UserDao.findById(id);

			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reset.jsp");
			dispatcher.forward(request, response);

		} else if (!(password.equals(password2))) {
			request.setAttribute("ErrMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reset.jsp");
			dispatcher.forward(request, response);

		}
		if (password.equals("") && password.equals("")) {
			UserDao Dao = new UserDao();
			Dao.updateUser(name, birthDate, id);
			response.sendRedirect("UserListServlet");

		} else {

			UserDao userDao = new UserDao();
			userDao.updateUser(password, name, birthDate, id);
			response.sendRedirect("UserListServlet");
		}

	}
}
