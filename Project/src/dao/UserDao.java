package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			String source = password;
			Charset charset = StandardCharsets.UTF_8;
			String algorithm = "MD5";
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, result);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {

			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void registUser(String loginId, String password, String name, String birthDate) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();

			String sql = "INSERT INTO user (login_id,password,name,birth_date,create_date,update_date) VALUES (?,?,?,?,now(),now())";

			String source = password;
			Charset charset = StandardCharsets.UTF_8;
			String algorithm = "MD5";
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, result);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);
			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}

			}
		}
	}



	public static User findById(String id) {
		Connection conn = null;

		try {

			conn = DBmanager.getConnection();

			String sql = "SELECT id,login_id,name, birth_date,create_date,update_date FROM user WHERE id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int Id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");

			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(Id, loginId, name, birthDate, createDate, updateDate);
			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public void updateUser(String password, String name, String birthDate, String id) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();

			String sql = "UPDATE user SET password = ?,name = ?,birth_date = ?,update_date = now() WHERE id = ?";

			String source = password;
			Charset charset = StandardCharsets.UTF_8;
			String algorithm = "MD5";
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, result);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, id);
			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}

			}
		}
	}
	public User findByLoginId(String loginId) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";


			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginID = rs.getString("login_id");
			return new User(loginID);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public void updateUser (String name, String birthDate, String id) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();

			String sql = "UPDATE user SET name = ?,birth_date = ?,update_date = now() WHERE id = ?";



			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, id);
			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}

			}
		}
	}
	public void deleteUser(String id) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();

			String sql = "DELETE FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<User> searchUser(String loginIdP, String name, String startDate, String endDate) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {

			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			if (!loginIdP.equals("")) {
				sql += " AND login_id = '" + loginIdP + "'";

			}

			if (!name.equals("")) {
				sql += " AND name LIKE '%" + name + "%'";

			}

			if (!startDate.equals("")) {
				sql += " AND birth_date >= '" + startDate + "'";

			}

			if (!endDate.equals("")) {
				sql += " AND birth_date <= '" + endDate + "'";

			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String Name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, Name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}
}
